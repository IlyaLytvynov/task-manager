import { Task } from './task';
export interface Student {
  id: string;
  name: string;
  mail: string;
  completedTasks: Task[];
  incompleteTasks: Task[];
}
