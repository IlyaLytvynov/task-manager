import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

export const TEST_IDS = {
  NATIVE_INPUT: 'check-box-native-input',
  CUSTOM_CHECK_BOX: 'check-box-custom_check_box',
  LABEL: 'check-box-custom_label',
  TITLE: 'check-box-custom_title'
};

@Component({
  selector: 'app-check-box',
  template: `
    <label [for]="htmlId" class="check-box" [attr.data-test-id]="TEST_IDS.LABEL">
      <input [id]="htmlId" [attr.data-test-id]="TEST_IDS.NATIVE_INPUT" class="check-box__native-input" [checked]="isChecked" (change)="changeHandler($event)" type="checkbox">
      <span class="check-box__input" [attr.data-test-id]="TEST_IDS.CUSTOM_CHECK_BOX" [ngClass]="{'check-box__input_checked': isChecked}"></span>
      <span *ngIf="title!== undefined" [attr.data-test-id]="TEST_IDS.TITLE"  class="check-box__title" [ngClass]="{'check-box__title_checked': isChecked }">
        {{title}}
      </span>
    </label>
  `,
  styleUrls: ['./check-box.component.scss']
})
export class CheckBoxComponent implements OnInit {

  htmlId: string = `checkbox-${Math.floor(Math.random() * 999)}`;
  TEST_IDS = TEST_IDS;
  @Input() title: string|undefined;
  @Input() isChecked: boolean = false;
  @Output() onChange: EventEmitter<Event> = new EventEmitter();

  changeHandler(e: Event) {
    this.onChange.emit(e)
  }
  constructor() { }

  ngOnInit(): void {
  }

}
