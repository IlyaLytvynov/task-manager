import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CheckBoxComponent, TEST_IDS } from './check-box.component';

describe('CheckBoxComponent', () => {
  let component: CheckBoxComponent;
  let fixture: ComponentFixture<CheckBoxComponent>;

  const setupTest = ({title, isChecked}: {title: string, isChecked: boolean}) => {
    fixture = TestBed.createComponent(CheckBoxComponent);
    component = fixture.componentInstance;
    component.isChecked = isChecked

    component.title = title
    fixture.detectChanges();
  }

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CheckBoxComponent ]
    })
    .compileComponents();
  });

  it('should create', () => {
    setupTest({title: 'Hello world', isChecked: true})
    expect(component).toBeTruthy();
  });

  it('should render title if it provided and mark check box as checked', () => {
    const expectedTitle = 'Hello world'
    setupTest({title: expectedTitle, isChecked: true})

    const checkBoxElement: HTMLElement = fixture.nativeElement;
    const checkBoxInput = checkBoxElement.querySelector(`[data-test-id="${TEST_IDS.CUSTOM_CHECK_BOX}"]`) as HTMLSpanElement
    const title = checkBoxElement.querySelector(`[data-test-id="${TEST_IDS.TITLE}"]`) as HTMLSpanElement
    expect(checkBoxInput.classList.contains('check-box__input_checked')).toBe(true)
    expect(title.textContent?.trim()).toBe(expectedTitle)
  })
});
