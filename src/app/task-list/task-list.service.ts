import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';
import { Task, Student } from '../shared/model';

const LOCAL_STORAGE_KEY = 'STUDENTS';

@Injectable({
  providedIn: 'root',
})
export class TaskListService {
  constructor() {}

  private readonly _studentData$: BehaviorSubject<Student[]> =
    new BehaviorSubject([] as Student[]);

  public get allStudentTasks(): BehaviorSubject<Student[]> {
    return this._studentData$;
  }

  public completeTask(studentId: string, taskId: string) {
    const allStudents = this._studentsData;
    const targetStudent = allStudents.find(
      (student: Student) => student.id === studentId
    );

    if (targetStudent === undefined) {
      console.warn(`Target studet is undefined ${studentId}`)
      return;
    }
    const taskToUpdate = targetStudent.incompleteTasks.find(
      (task: Task) => task.id === taskId
    );

    if (taskToUpdate === undefined) {
      console.warn(`taskToUpdate is undefined ${studentId} ${taskId}`)
      return;
    }

    targetStudent.incompleteTasks = targetStudent.incompleteTasks.filter(
      (task: Task) => task.id !== taskId
    );

    taskToUpdate.done = true;
    targetStudent.completedTasks = [
      ...targetStudent.completedTasks,
      taskToUpdate,
    ];
    this.notifySubscribers(allStudents);
  }

  public deleteStudent(studentId: string) {
    const allStudents = this._studentsData;
    const otherStudents = allStudents.filter(
      (student: Student) => student.id !== studentId
    );
    this.notifySubscribers(otherStudents);
  }

  public makeStudentData(): void {
    const fromLocalStorage = localStorage.getItem(LOCAL_STORAGE_KEY);
    let data;
    if (fromLocalStorage) {
      try {
        data = JSON.parse(fromLocalStorage);
      } catch(e) {
        console.warn(e)
        data = this.createInitialData()
      }

    } else {
      data = this.createInitialData()
    }
    this._studentData$.next(data);
  }

  private createInitialData() {
    return [1, 2, 3, 4, 5, 6, 7].map((n) => {
      return {
        id: `std-${n}`,
        name: `Student ${n}`,
        mail: `student${n}@mail.com`,
        completedTasks: this.makeCompleteTaskList(),
        incompleteTasks: this.makeIncompleteTaskList(),
      };
    });
  }

  private notifySubscribers(data: Student[]) {
    this._studentData$.next(data);
    this.syncStorage(data);
  }

  private syncStorage(data: Student[]) {
    localStorage.setItem(LOCAL_STORAGE_KEY, JSON.stringify(data));
  }

  private makeIncompleteTaskList(): Task[] {
    return [1, 2, 3].map((n) => {
      return { id: `tsk-${n}`, name: `Task ${n}`, done: false };
    });
  }

  private makeCompleteTaskList(): Task[] {
    return [4, 5].map((n) => {
      return { id: `tsk-${n}`, name: `Task ${n}`, done: true };
    });
  }

  private get _studentsData() {
    return this._studentData$.getValue();
  }
}
