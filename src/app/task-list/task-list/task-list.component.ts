import { Component, OnInit } from '@angular/core';
import { Student, Task } from '../../shared/model';
import { TaskListService } from '../task-list.service';

@Component({
  selector: 'app-task-list',
  template: `
    <div class="task-list container">
      <h2 class="task-list__title">Student Task List</h2>
      <div class="grid-container">
        <div
          class="task-list__item col-4 col-md-6 col-xs-12"
          *ngFor="let student of allStudents"
        >
          <app-student-card
            [name]="student.name"
            [mail]="student.mail"
            [completedTasks]="student.completedTasks"
            [incompleteTasks]="student.incompleteTasks"
            (onComplete)="onCompleteTask(student, $event)"
            (onStudentDelete)="onStudentDelete(student)"
          ></app-student-card>
        </div>
      </div>
    </div>
  `,
  styleUrls: ['./task-list.component.scss'],
})
export class TaskListComponent implements OnInit {
  constructor(private taskListService: TaskListService) {}

  private _data: Student[] = [];

  get allStudents() {
    return this._data;
  }

  onCompleteTask(student: Student, task: Task) {
    this.taskListService.completeTask(student.id, task.id);
  }

  onStudentDelete(student: Student) {
    this.taskListService.deleteStudent(student.id);
  }

  ngOnInit(): void {
    this.taskListService.allStudentTasks.subscribe((students) => {
      this._data = students;
    });
    this.taskListService.makeStudentData();
  }
  ngOnDestroy(): void {
    this.taskListService.allStudentTasks.unsubscribe();
  }
}
