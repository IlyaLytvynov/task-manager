import { TestBed } from '@angular/core/testing';
import { Student } from '../shared/model';

import { TaskListService } from './task-list.service';

describe('TaskListService', () => {
  let service: TaskListService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TaskListService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should notify when students are chnaged', () => {
    const spy = jasmine.createSpy();
    service.allStudentTasks.subscribe(spy);
    service.makeStudentData();
    expect(spy).toHaveBeenCalled();
  });

  it('should create initial students', () => {
    let callCount = 0;
    service.makeStudentData();
    const listener = (students: Student[]) => {
      if (callCount === 2) {
        expect(students.length).toBe(7);
      }
      callCount += 1;
    };
    service.allStudentTasks.subscribe(listener);
  });

  it('should delete student by id', () => {
    service.makeStudentData();
    let allStudents: Student[] = service.allStudentTasks.getValue();
    const beforeDelete = allStudents.length
    service.deleteStudent(allStudents[0].id)
    expect(service.allStudentTasks.getValue().length).toBe(beforeDelete - 1)
  })
});
