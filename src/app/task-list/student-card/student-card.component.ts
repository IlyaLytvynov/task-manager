import {
  Component,
  Input,
  OnInit,
  Output,
  EventEmitter,
  OnChanges,
} from '@angular/core';
import { Task } from '../../shared/model';

@Component({
  selector: 'app-student-card',
  template: `
    <div class="student-card">
      <div class="student-card__header">
        <div class="student-card__avatar">S1</div>
        <div class="student-card__title">
          <div class="student-card__name">
            {{ name }}
          </div>
          <div class="student-card__email">
            {{ mail }}
          </div>
        </div>
        <button
          class="student-card__delete-btn"
          (click)="onStudentDelete.emit()"
        ></button>
      </div>
      <ul class="student-card__task-list ">
        <li *ngFor="let task of allTasks" class="student-card__task">
          <app-check-box
            [title]="task.name"
            [id]="task.id"
            [isChecked]="task.done"
            (onChange)="onTaskChange(task)"
          ></app-check-box>
        </li>
      </ul>
    </div>
  `,
  styleUrls: ['./student-card.component.scss'],
})
export class StudentCardComponent implements OnInit, OnChanges {
  @Input() name: string | undefined;
  @Input() mail: string | undefined;
  @Input() incompleteTasks: Task[] = [];
  @Input() completedTasks: Task[] = [];

  @Output() onComplete = new EventEmitter();
  @Output() onStudentDelete = new EventEmitter();

  private _allTasks: Task[] = [];

  constructor() {}

  ngOnInit(): void {
    this.setTasks();
  }

  ngOnChanges(): void {
    this.setTasks();
  }

  private setTasks() {
    this._allTasks = [...this.completedTasks, ...this.incompleteTasks];
  }

  get allTasks() {
    return this._allTasks;
  }

  onTaskChange = (task: Task) => {
    this.onComplete.emit(task);
  };
}
